import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_apps_flutter/applications/movie/category_cubit.dart';
import 'package:movie_apps_flutter/applications/movie/popular_movie_cubit.dart';
import 'package:movie_apps_flutter/applications/movie/search_movie_cubit.dart';
import 'package:movie_apps_flutter/presentations/widgets/item_movie.dart';

class SearchScreen extends StatelessWidget {
  SearchScreen({super.key});
  Timer? _debounce;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocProvider(
          create: (context) => SearchMovieCubit(),
          child: BlocBuilder<SearchMovieCubit, SearchMovieState>(
            builder: (context, state) {
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 24, right: 24, bottom: 24, top: 24),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: const [BoxShadow(blurRadius: 1)],
                      ),
                      child: TextField(
                        autofocus: true,
                        textInputAction: TextInputAction.search,
                        keyboardType: TextInputType.text,
                        style: const TextStyle(color: Colors.white),
                        onSubmitted: (value) {
                          context.read<SearchMovieCubit>().searchMovie(value);
                        },
                        onChanged: (String query) {
                          if (_debounce?.isActive ?? false) _debounce?.cancel();
                          _debounce =
                              Timer(const Duration(milliseconds: 500), () {
                                context.read<SearchMovieCubit>().searchMovie(query);
                          });
                        },
                        decoration: InputDecoration(
                          fillColor: const Color(0xFF36394A),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(16),
                            borderSide: const BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          hintText: "Search",
                          hintStyle: const TextStyle(color: Colors.white24),
                          prefixIcon: InkResponse(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: const Icon(
                              Icons.chevron_left,
                              color: Colors.white60,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(child: Builder(builder: (context) {
                    if (state is SearchMovieLoading) {
                      return const Center(
                          child: CircularProgressIndicator(
                        color: Colors.white70,
                      ));
                    } else if (state is SearchMovieLoaded) {
                      if (state.movies.isEmpty) {
                        return Center(
                          child: Text(
                            "Movie not found",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(color: Colors.white),
                          ),
                        );
                      } else {
                        return GridView.builder(
                            padding: const EdgeInsets.only(
                                left: 12, right: 12, bottom: 24),
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    childAspectRatio: 2 / 3,
                                    mainAxisSpacing: 12,
                                    crossAxisSpacing: 12),
                            itemCount: state.movies.length,
                            itemBuilder: (context, index) {
                              return ItemMovie(state.movies[index]);
                            });
                      }
                    }
                    return const SizedBox();
                  }))
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
