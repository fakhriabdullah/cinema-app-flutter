import 'package:flutter/material.dart';
import 'package:movie_apps_flutter/domains/movie/movie.dart';
import 'package:movie_apps_flutter/presentations/movie_screen.dart';

class ItemMovie extends StatelessWidget {
  final Movie movie;
  const ItemMovie(this.movie, {super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(12),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => MovieScreen(movie),
            ),
          );
        },
        child: Ink(
          width: 150,
          child: Image.network(
            movie.image ?? "",
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
