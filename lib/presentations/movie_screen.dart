import 'package:flutter/material.dart';
import 'package:movie_apps_flutter/domains/movie/movie.dart';

class MovieScreen extends StatelessWidget {
  final Movie movie;
  const MovieScreen(this.movie, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 86,
        title: const Text("Detail"),
        centerTitle: true,
        leadingWidth: 80,
        leading: Container(
          margin: const EdgeInsets.only(top: 16, left: 24, bottom: 16),
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Ink(
              decoration: BoxDecoration(
                color: const Color(0xFF36394A),
                borderRadius: BorderRadius.circular(12),
              ),
              child: const Center(child: Icon(Icons.chevron_left)),
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: ListView(
          padding: const EdgeInsets.all(24),
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(24),
              child: Image.network(
                movie.image ?? "",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              width: double.maxFinite,
              margin: const EdgeInsets.only(top: 18),
              child: Text(
                movie.trackName ?? "",
                style: Theme.of(context).textTheme.headline5?.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 16, bottom: 16),
              child: SizedBox(
                height: 38,
                child: ListView(
                  primary: false,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  children: [
                    Ink(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 16),
                      decoration: BoxDecoration(
                          color: const Color(0xFF36394A),
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                        child: Text(
                          "Horror",
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Text(
              "Synopsis Ironman",
              style: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 16),
            Text(
              movie.description ?? "",
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium
                  ?.copyWith(color: Colors.white),
            ),
            const SizedBox(height: 32),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Theme.of(context).primaryColor,
                    minimumSize: const Size(100, 55),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12))),
                onPressed: () {},
                child: const Text("Buy Ticket"))
          ],
        ),
      ),
    );
  }
}
