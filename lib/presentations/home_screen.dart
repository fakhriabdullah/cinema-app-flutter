import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_apps_flutter/applications/movie/category_cubit.dart';
import 'package:movie_apps_flutter/applications/movie/popular_movie_cubit.dart';
import 'package:movie_apps_flutter/presentations/search_screen.dart';
import 'package:movie_apps_flutter/presentations/widgets/item_movie.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: MultiBlocProvider(
          providers: [
            BlocProvider(create: (context) => CategoryCubit()),
            BlocProvider(create: (context) => PopularMovieCubit()),
          ],
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(24),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 8,
                      clipBehavior: Clip.hardEdge,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50)),
                      child: Image.network(
                          "https://randomuser.me/api/portraits/men/76.jpg"),
                    ),
                    const SizedBox(width: 12),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Welcome",
                            style: Theme.of(context)
                                .textTheme
                                .labelSmall
                                ?.copyWith(color: Colors.white70),
                          ),
                          const SizedBox(height: 4),
                          Text(
                            "Fakhri Abdullah",
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                    InkResponse(
                      onTap: () {},
                      child: const Icon(
                        Icons.notifications,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 24, right: 24, bottom: 24),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SearchScreen()),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      boxShadow: const [BoxShadow(blurRadius: 1)],
                    ),
                    child: TextFormField(
                      autofocus: false,
                      enabled: false,
                      keyboardType: TextInputType.text,
                      style: const TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        fillColor: const Color(0xFF36394A),
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        hintText: "Search",
                        hintStyle: const TextStyle(color: Colors.white24),
                        prefixIcon: const Icon(
                          Icons.search,
                          color: Colors.white60,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 24, right: 24, bottom: 16),
                child: Text(
                  "Categories",
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
              BlocBuilder<CategoryCubit, CategoryState>(
                builder: (context, state) {
                  if (state is CategoryLoaded) {
                    return SizedBox(
                      height: 38,
                      child: ListView.separated(
                        primary: false,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        itemCount: state.categories.length,
                        separatorBuilder: (context, index) {
                          return const SizedBox(width: 12);
                        },
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              context.read<CategoryCubit>().onSelectedChange(
                                    state.categories,
                                    state.categories[index],
                                  );
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 18),
                              decoration: BoxDecoration(
                                  color:
                                      state.selected == state.categories[index]
                                          ? Theme.of(context).primaryColor
                                          : const Color(0xFF36394A),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Center(
                                child: Text(
                                  state.categories[index],
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall
                                      ?.copyWith(color: Colors.white),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return const SizedBox();
                  }
                },
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 24, right: 24, bottom: 16, top: 24),
                child: Text(
                  "Popular",
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
              BlocBuilder<PopularMovieCubit, PopularMovieState>(
                builder: (context, state) {
                  if (state is PopularMovieLoaded) {
                    return SizedBox(
                      height: 200,
                      child: ListView.separated(
                        primary: false,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        itemCount: state.movies.length,
                        separatorBuilder: ((context, index) {
                          return const SizedBox(width: 18);
                        }),
                        itemBuilder: (context, index) {
                          return ItemMovie(state.movies[index]);
                        },
                      ),
                    );
                  } else {
                    return const SizedBox();
                  }
                },
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        clipBehavior: Clip.hardEdge,
        margin: const EdgeInsets.only(left: 24, right: 24, bottom: 24),
        height: 80,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(14)),
        child: BottomNavigationBar(
          backgroundColor: const Color(0xff1B1A1F),
          showSelectedLabels: false,
          showUnselectedLabels: false,
          fixedColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.white,
          items: const [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home_filled,
                ),
                label: "Home"),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.movie,
                ),
                label: "Ticket"),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.person,
                ),
                label: "Profile"),
          ],
        ),
      ),
    );
  }
}
