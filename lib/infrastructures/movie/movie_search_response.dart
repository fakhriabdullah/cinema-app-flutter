import 'package:movie_apps_flutter/infrastructures/movie/movie_dto.dart';

class MovieSearchResponse {
  int? resultCount;
  List<MovieDto>? results;

  MovieSearchResponse({this.resultCount, this.results});

  MovieSearchResponse.fromJson(Map<String, dynamic> json) {
    resultCount = json['resultCount'];
    if (json['results'] != null) {
      results = <MovieDto>[];
      json['results'].forEach((v) {
        results!.add(new MovieDto.fromJson(v));
      });
    }
  }
}