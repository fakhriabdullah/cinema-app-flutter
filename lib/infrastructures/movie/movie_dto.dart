import '../../domains/movie/movie.dart';

class MovieDto extends Movie {
  MovieDto(
    int? trackId,
    String? trackName,
    String? description,
    String? image,
  ) : super(
          trackId: trackId,
          trackName: trackName,
          description: description,
          image: image,
        );

  factory MovieDto.fromJson(Map<String, dynamic> json) {
    String? image = json['artworkUrl100'];
    if (image != null) {
      image = image.replaceAll("100x100", "500x500");
    }
    return MovieDto(
      json["trackId"],
      json["trackName"],
      json["longDescription"],
      image,
    );
  }
}
