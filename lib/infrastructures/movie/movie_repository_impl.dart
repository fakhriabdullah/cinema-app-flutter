import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:movie_apps_flutter/domains/movie/movie.dart';
import 'package:movie_apps_flutter/domains/movie/movie_repository.dart';
import 'package:movie_apps_flutter/infrastructures/movie/movie_search_response.dart';
import 'package:movie_apps_flutter/util/api_client.dart';

class MovieRepositoryImpl extends MovieRepository {
  Dio api = Client().init();
  @override
  Future<List<Movie>> getPopularMovie() async {
    await Future.delayed(const Duration(milliseconds: 500));
    List<Movie> result = [];

    //movie 1
    result.add(Movie(
        trackId: 661107687,
        trackName: "Iron Man 3",
        description:
            "The studio that brought you Marvel's The Avengers unleashes the best Iron Man adventure yet with this must-own, global phenomenon starring Robert Downey Jr. and Gwyneth Paltrow. When Tony Stark/Iron Man finds his entire world reduced to rubble, he must use all his ingenuity to survive, destroy his enemy and somehow protect those he loves. But a soul-searching question haunts him: Does the man make the suit… or does the suit make the man?",
        image:
            "https://is1-ssl.mzstatic.com/image/thumb/Video62/v4/c7/5a/a2/c75aa277-a2ef-b95f-9c7c-5f8d5ee70443/contsched.dbkaetoq.lsr/500x500bb.jpg"));
    result.add(Movie(
        trackId: 1104040170,
        trackName: "Captain America: Civil War",
        description:
            "Marvel's Captain America: Civil War finds Steve Rogers leading the newly formed team of Avengers in their continued efforts to safeguard humanity. But after another incident involving the Avengers results in collateral damage, political pressure mounts to install a system of accountability, headed by a governing body to oversee and direct the team. The new status quo fractures the Avengers, resulting in two camps—one led by Steve Rogers and his desire for the Avengers to remain free to defend humanity without government interference, and the other following Tony Stark's surprising decision to support government oversight and accountability.",
        image:
            "https://is1-ssl.mzstatic.com/image/thumb/Video128/v4/bb/c8/a6/bbc8a623-fefa-9ab2-d32f-345de954e48a/contsched.kwpdgzmz.lsr/500x500bb.jpg"));

    result.add(Movie(
        trackId: 661107687,
        trackName: "Iron Man 3",
        description:
            "The studio that brought you Marvel’s The Avengers unleashes the best Iron Man adventure yet with this must-own, global phenomenon starring Robert Downey Jr. and Gwyneth Paltrow. When Tony Stark/Iron Man finds his entire world reduced to rubble, he must use all his ingenuity to survive, destroy his enemy and somehow protect those he loves. But a soul-searching question haunts him: Does the man make the suit… or does the suit make the man?",
        image:
            "https://is1-ssl.mzstatic.com/image/thumb/Video62/v4/c7/5a/a2/c75aa277-a2ef-b95f-9c7c-5f8d5ee70443/contsched.dbkaetoq.lsr/500x500bb.jpg"));

    result.add(Movie(
        trackId: 661107687,
        trackName: "Iron Man 3",
        description:
            "The studio that brought you Marvel’s The Avengers unleashes the best Iron Man adventure yet with this must-own, global phenomenon starring Robert Downey Jr. and Gwyneth Paltrow. When Tony Stark/Iron Man finds his entire world reduced to rubble, he must use all his ingenuity to survive, destroy his enemy and somehow protect those he loves. But a soul-searching question haunts him: Does the man make the suit… or does the suit make the man?",
        image:
            "https://is1-ssl.mzstatic.com/image/thumb/Video62/v4/c7/5a/a2/c75aa277-a2ef-b95f-9c7c-5f8d5ee70443/contsched.dbkaetoq.lsr/500x500bb.jpg"));

    result.add(Movie(
        trackId: 661107687,
        trackName: "Iron Man 3",
        description:
            "The studio that brought you Marvel’s The Avengers unleashes the best Iron Man adventure yet with this must-own, global phenomenon starring Robert Downey Jr. and Gwyneth Paltrow. When Tony Stark/Iron Man finds his entire world reduced to rubble, he must use all his ingenuity to survive, destroy his enemy and somehow protect those he loves. But a soul-searching question haunts him: Does the man make the suit… or does the suit make the man?",
        image:
            "https://is1-ssl.mzstatic.com/image/thumb/Video62/v4/c7/5a/a2/c75aa277-a2ef-b95f-9c7c-5f8d5ee70443/contsched.dbkaetoq.lsr/500x500bb.jpg"));

    return result;
  }

  @override
  Future<List<Movie>> searchMovie(String query) async {
    try {
      query = query.replaceAll(" ", "+");
      var response = await api
          .get("search", queryParameters: {"term": query, "entity": "movie"});
      var movieSearchResponse =
          MovieSearchResponse.fromJson(jsonDecode(response.data.trim()));
      return movieSearchResponse.results ?? [];
    } on DioException catch (e) {
      log(e.toString());
      return [];
    }
  }

  @override
  Future<List<String>> getCategories() {
    return Future.value(["Horor", "Romance", "Comedy", "Sci-fi", "Triller"]);
  }
}
