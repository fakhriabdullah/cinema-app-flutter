import 'package:movie_apps_flutter/domains/user/user.dart';
import 'package:movie_apps_flutter/domains/user/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  @override
  Future<bool> login(String username, String password) async {
    await Future.delayed(const Duration(seconds: 1));
    return (username == "fakhri" && password == "12345678");
  }

  @override
  Future<User> getUser() {
    return Future.value(User(
        username: "fakhri",
        avatar: "https://randomuser.me/api/portraits/men/76.jpg"));
  }
}
