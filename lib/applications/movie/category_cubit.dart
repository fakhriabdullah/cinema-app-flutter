import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:movie_apps_flutter/domains/movie/movie.dart';
import 'package:movie_apps_flutter/domains/movie/movie_repository.dart';
import 'package:movie_apps_flutter/infrastructures/movie/movie_repository_impl.dart';

part 'category_state.dart';

class CategoryCubit extends Cubit<CategoryState> {
  MovieRepository repository = MovieRepositoryImpl();
  CategoryCubit() : super(CategoryInitial()){
    loadCategory();
  }

  void loadCategory()async {
    try{
      var resp = await repository.getCategories();
      emit(CategoryLoaded(resp, resp.first));
    }catch(e){
      log(e.toString());
      emit(CategoryLoaded(const [], ""));
    }
  }

  void onSelectedChange(List<String> categories, String selected){
    emit(CategoryLoaded(categories, selected));
  }
}
