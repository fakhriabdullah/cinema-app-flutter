part of 'search_movie_cubit.dart';

@immutable
abstract class SearchMovieState {}

class SearchMovieInitial extends SearchMovieState {}

class SearchMovieLoading extends SearchMovieState {}

class SearchMovieLoaded extends SearchMovieState {
  final List<Movie> movies;
  SearchMovieLoaded(this.movies);
}

