import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:movie_apps_flutter/domains/movie/movie.dart';
import 'package:movie_apps_flutter/domains/movie/movie_repository.dart';
import 'package:movie_apps_flutter/infrastructures/movie/movie_repository_impl.dart';

part 'popular_movie_state.dart';

class PopularMovieCubit extends Cubit<PopularMovieState> {
  MovieRepository repository = MovieRepositoryImpl();
  PopularMovieCubit() : super(PopularMovieInitial()){
    loadMovie();
  }

  void loadMovie()async {
    emit(PopularMovieLoading());
    try{
      var resp = await repository.getPopularMovie();
      emit(PopularMovieLoaded(resp));
    }catch(e){
      log(e.toString());
      emit(PopularMovieLoaded(const []));
    }
  }
}
