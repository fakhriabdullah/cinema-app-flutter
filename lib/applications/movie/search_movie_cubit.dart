import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:movie_apps_flutter/domains/movie/movie.dart';
import 'package:movie_apps_flutter/domains/movie/movie_repository.dart';
import 'package:movie_apps_flutter/infrastructures/movie/movie_repository_impl.dart';

part 'search_movie_state.dart';

class SearchMovieCubit extends Cubit<SearchMovieState> {
  MovieRepository repository = MovieRepositoryImpl();
  SearchMovieCubit() : super(SearchMovieInitial());

  void searchMovie(String query)async {
    emit(SearchMovieLoading());
    try{
      var resp = await repository.searchMovie(query);
      emit(SearchMovieLoaded(resp));
    }catch(e){
      log(e.toString());
      emit(SearchMovieLoaded(const []));
    }
  }
}
