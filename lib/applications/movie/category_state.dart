part of 'category_cubit.dart';

@immutable
abstract class CategoryState {
}

class CategoryInitial extends CategoryState {}


class CategoryLoaded extends CategoryState {
  final List<String> categories;
  final String selected;
  CategoryLoaded(this.categories, this.selected);
}

