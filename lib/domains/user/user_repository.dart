import 'package:movie_apps_flutter/domains/user/user.dart';

abstract class UserRepository {
  Future<bool> login(String username, String password);
  Future<User> getUser();
}