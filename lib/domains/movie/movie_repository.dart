import 'package:movie_apps_flutter/domains/movie/movie.dart';

abstract class MovieRepository {
  Future<List<Movie>> getPopularMovie();
  Future<List<String>> getCategories();
  Future<List<Movie>> searchMovie(String query);
}