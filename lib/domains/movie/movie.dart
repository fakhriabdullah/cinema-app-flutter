class Movie {
  final int? trackId;
  final String? trackName;
  final String? description;
  final String? image;
  Movie({
    required this.trackId,
    required this.trackName,
    required this.description,
    required this.image,
  });
}
