import 'package:flutter/material.dart';
import 'package:movie_apps_flutter/presentations/home_screen.dart';
import 'package:movie_apps_flutter/presentations/login_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cinema App',
      theme: ThemeData(
          primaryColor: const Color(0xFF6A3AFF),
          scaffoldBackgroundColor: const Color(0xFF25252F),
          appBarTheme: const AppBarTheme(
            backgroundColor: Color(0xFF25252F),
            elevation: 0,
            centerTitle: true,
          )),
      debugShowCheckedModeBanner: false,
      home: const LoginScreen(),
    );
  }
}
